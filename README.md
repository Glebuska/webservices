# Web-services 

RESTful API service to help students

The system is designed to support the activities of the internal network of tutors.
It is necessary to maintain a register of tutors who are able to conduct one-time consultations and short-term training in one or several disciplines. Unsuccessful students will be able to choose the appropriate assistance option by finding the right tutors in the register. In addition to contact information, tutors during registration can indicate their preferences for the place and time of classes.
The system will also record data on the facts and the success of the lessons, in order to accumulate statistics on this basis to assess the success of the entire network and individual tutors, build a rating of tutors, and see problems with teaching certain disciplines at the university.

# Install and build
This project use sbt to build project, so install it. Next use follow commands:
```
git clone https://gitlab.com/Glebuska/webservices
cd webservices
sbt run
```

## Using Docker
```
git clone https://gitlab.com/Glebuska/webservices
cd webservices
docker build -t web-services .
docker run -p 8080:8080 web-services
```
This service using port 8080.

# Usage 
This service uses JWT authentication with Akka HTTP. Use Postman or another tools to communicate with service.
NOTE: For this pet-project I use h2 database. Database keeps data in memory, so after and of session all data will be remove.
## Example
See request.http example file
