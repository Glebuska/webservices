package studServices

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{Route, RouteConcatenation}
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.H2Profile.api._
import studServices.api.{AdminApi, ServerExceptionHandler, StudentApi}
import studServices.logic.InitScript.initDB
import studServices.logic.structures.{AdminImp, Student}
import studServices.storage.{ServiceStorage, ServiceStorageImp}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

object ServiceHttpApp extends {
  implicit val ac: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = ac.dispatcher

  def main(args: Array[String]): Unit = {
    Await.result(ServiceApp().start(), Duration.Inf)
  }

  case class ServiceApp()(implicit ac: ActorSystem, ec: ExecutionContext) extends LazyLogging {
    private val url = "0.0.0.0"
    private val db = Database.forConfig("h2mem")
    private val admin = AdminImp()
    private val student = Student(2, "IVANOV_ANATOLY", "qwerty12345", "Ivanov", "Ananoly", "home", None, 2)
    private val studServices: ServiceStorage = new ServiceStorageImp(db)
    private val adminRoute: AdminApi = new AdminApi(admin, studServices)
    private val studentRoute: StudentApi = new StudentApi(student, studServices)
    private val routes = Route.seal(
      RouteConcatenation.concat(
        studentRoute.route,
        adminRoute.route
      )
    )(
      exceptionHandler = ServerExceptionHandler.exceptionHandler // Обрабатывает ошибки, возникающие при обработке запроса
    )
    private val querySeq = DBIO.seq(initDB)
    def start(): Future[Http.ServerBinding] = for {
      _ <- db.run(querySeq)
      res <- Http()
        .newServerAt(url, 8080)
        .bind(routes)
        .andThen { case b => logger.info(s"server started at: $b") }
    } yield res
  }

}