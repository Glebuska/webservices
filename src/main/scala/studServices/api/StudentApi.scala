package studServices.api

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import io.circe.generic.auto.exportEncoder
import studServices.api.AuthenticationUtil.{LoginRequest, authenticatedStudent}
import studServices.logic.JsonPrepare.jsonRegistrationRequestDecoder
import studServices.logic.structures.{RegistrationRequest, Student}
import studServices.storage.ServiceStorage

import scala.util.Failure

//POST http://localhost:8080/student/login
//POST http://localhost:8080/student/registry
//GET http://localhost:8080/student/orders
//GET http://localhost:8080/student/orders/active
//POST http://localhost:8080/student/send/mentor
//GET http://localhost:8080/student/find/subject


class StudentApi(student: Student, studStorage: ServiceStorage) {

  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
  import studServices.api.AuthenticationUtil.{createJWT, setClaims}
  import studServices.logic.JsonPrepare.jsonLoginRequestDecoder

  private val tokenExpiryPeriodInDays = 1

  val route: Route = pathPrefix("student") {
    concat(
      path("login") {
        entity(as[LoginRequest]) {
          case lr@LoginRequest(_, _, "student") =>
            val claims = setClaims(lr.login, tokenExpiryPeriodInDays, "student")
            respondWithHeader(RawHeader("Access-Token", createJWT(claims))) {
              complete(StatusCodes.OK -> "Login good")
            }
          case LoginRequest(_, _, _) => complete(StatusCodes.Unauthorized -> "Error login")
        }
      },
      (post & path("registry")) {
        entity(as[RegistrationRequest]) { rr =>
            onComplete(studStorage.
              registryStudent(rr.login,
                rr.password,
                rr.lastName,
                rr.firstName,
                rr.placePrefer)) {
              case scala.util.Success(student) =>
                val claims = setClaims(student.login, tokenExpiryPeriodInDays, "student")
                respondWithHeader(RawHeader("Access-Token", createJWT(claims))){
                  complete(StatusCodes.OK, student)
                }
              case Failure(ex) => complete(StatusCodes.BadRequest, s"An error occurred: ${ex.getMessage}")
            }
        }
      },
      authenticatedStudent { claims =>
        claims.getOrElse("lvlAccess", "") match {
          case "student" => concat(pathPrefix("orders") {
            concat(
              (get & path("active")) {
                  onSuccess(student.findActiveOrders(studStorage)) { orders =>
                    complete(StatusCodes.OK, orders)
                  }
              },
              (get & pathEnd) {
                  onSuccess(student.findOrders(studStorage)) { orders =>
                    complete(StatusCodes.OK, orders)
                }
              }
            )
          },
            (post & path("send" / IntNumber)){ mentorID =>
                onComplete(student.sendRequest(mentorID, studStorage)){
                  case Failure(ex) => complete(StatusCodes.BadRequest, s"An error occurred: ${ex.getMessage}")
                  case scala.util.Success(order) => complete(StatusCodes.OK, order)
                }
            },
            (get & path("find" / Segment)){subject: String =>
                onSuccess(student.findPartner(subject, studStorage)){partners =>
                  complete(StatusCodes.OK, partners)
                }
            }
          )
          case _ => complete(StatusCodes.Unauthorized -> "Unauthorized")
        }
      }
    )
  }
}