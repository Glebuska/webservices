package studServices.api

import java.util.concurrent.TimeUnit

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directive1
import akka.http.scaladsl.server.Directives._
import authentikat.jwt.{JsonWebToken, JwtClaimsSet, JwtClaimsSetMap}

object AuthenticationUtil {
  import authentikat.jwt.JwtHeader



  private val secretKey               = "super_secret_key"
  private val header: JwtHeader = JwtHeader("HS256")
  val AccessTokenHeaderName = "Access-Token"

  final case class LoginRequest(login: String, password: String, accessLvl: String)

  def createJWT(claims: JwtClaimsSetMap): String = JsonWebToken(header, claims, secretKey)
   def setClaims(username: String, expiryPeriodInDays: Long, lvlAccess: String): JwtClaimsSetMap = JwtClaimsSet(
    Map("user" -> username,
      "expiredAt" -> (System.currentTimeMillis() + TimeUnit.DAYS
        .toMillis(expiryPeriodInDays)),
      "lvlAccess" -> lvlAccess)
  )

  def authenticatedAdmin: Directive1[Map[String, Any]] =
    optionalHeaderValueByName("Authorization").flatMap {
      case Some(jwt) if isTokenExpired(jwt) =>
        complete(StatusCodes.Unauthorized -> "Token expired.")

      case Some(jwt) if !isAdminToken(jwt) =>
        complete(StatusCodes.Unauthorized -> "Access lvl error")

      case Some(jwt) if JsonWebToken.validate(jwt, secretKey) =>
        provide(getClaims(jwt).getOrElse(Map.empty[String, Any]))

      case _ => complete(StatusCodes.Unauthorized)
    }

  def authenticatedStudent: Directive1[Map[String, Any]] =
    optionalHeaderValueByName("Authorization").flatMap {
      case Some(jwt) if isTokenExpired(jwt) =>
        complete(StatusCodes.Unauthorized -> "Token expired.")

      case Some(jwt) if !isStudentToken(jwt) =>
        complete(StatusCodes.Unauthorized -> "Access lvl error")

      case Some(jwt) if JsonWebToken.validate(jwt, secretKey) =>
        provide(getClaims(jwt).getOrElse(Map.empty[String, Any]))

      case _ => complete(StatusCodes.Unauthorized -> "Nothing")
    }

  private def isAdminToken(jwt: String): Boolean = getClaims(jwt) match {
    case Some(claims) =>
      claims.get("lvlAccess") match {
        case Some(value) => value == "admin"
        case None        => false
      }
    case None => false
  }

  private def isStudentToken(jwt: String): Boolean = getClaims(jwt) match {
    case Some(claims) =>
      claims.get("lvlAccess") match {
        case Some(value) => value == "student"
        case None        => false
      }
    case None => false
  }


  private def isTokenExpired(jwt: String): Boolean = getClaims(jwt) match {
    case Some(claims) =>
      claims.get("expiredAt") match {
        case Some(value) => value.toLong < System.currentTimeMillis()
        case None        => false
      }
    case None => false
  }


  private def getClaims(jwt: String) = jwt match {
    case JsonWebToken(_, claims, _) => claims.asSimpleMap.toOption
    case _                          => None
  }

}