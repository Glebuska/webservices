package studServices.api

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import io.circe.generic.auto.exportEncoder
import studServices.api.AuthenticationUtil.authenticatedAdmin
import studServices.logic.structures.AdminImp
import studServices.storage.ServiceStorage

import scala.util.{Failure, Success}

//POST http://localhost:8080/admin/login
//GET http://localhost:8080/admin/orders
//GET http://localhost:8080/admin/orders/active
//GET http://localhost:8080/admin/orders/id
//GET http://localhost:8080/admin/students
//GET http://localhost:8080/admin/mentors
//DELETE http://localhost:8080/admin/mentor/id
//DELETE http://localhost:8080/admin/student/id

class AdminApi(admin: AdminImp, studStorage: ServiceStorage) {
  import AuthenticationUtil.{AccessTokenHeaderName, createJWT, setClaims}
  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
  import studServices.logic.JsonPrepare.{jsonAdminDecoder, jsonMentorEncoder, jsonStudentEncoder}
  private val tokenExpiryPeriodInDays = 1


  val route: Route = pathPrefix("admin") {
    concat(
      path("login") {
        entity(as[AdminImp]) {
          case lr @ AdminImp("admin", "admin") =>
            val claims = setClaims(lr.login, tokenExpiryPeriodInDays, "admin")
            respondWithHeader(RawHeader(AccessTokenHeaderName, createJWT(claims))) {
              complete(StatusCodes.OK)
            }
          case AdminImp(_, _) => complete(StatusCodes.Unauthorized -> "Admin login error")
        }
      },
      authenticatedAdmin { claims => claims.getOrElse("user", "") match {
        case "admin" => concat(pathPrefix("orders") {
          concat(
            path(IntNumber) { id =>
              get {
                onSuccess(admin.findOrdersById(id, studStorage)) { orders =>
                  complete(StatusCodes.OK, orders)
                }
              }
            },
            path("active") {
              get {
                onSuccess(admin.findActiveOrders(studStorage)) { orders =>
                  complete(StatusCodes.OK, orders)
                }
              }
            },
            get {
              onSuccess(admin.findOrders(studStorage)) { orders =>
                complete(StatusCodes.OK, orders)
              }
            }
          )
        },
          path("students") {
            get {
              onSuccess(admin.findStudents(studStorage)) { orders =>
                complete(StatusCodes.OK, orders)
              }
            }
          },
          path("mentors") {
            get {
              onSuccess(admin.findMentors(studStorage)) { orders =>
                complete(StatusCodes.OK, orders)
              }
            }
          },
          path("mentor" / IntNumber) {id =>
            delete{
              onComplete(admin.deleteMentor(id, studStorage)){
                case Success(mentorId) => complete(StatusCodes.OK, s"Mentor with id=$mentorId successful delete")
                case Failure(ex)    => complete(StatusCodes.BadRequest, s"An error occurred: ${ex.getMessage}")
              }
            }
          },
          path("student" / IntNumber){ id =>
            delete{
              onComplete(admin.deleteStudent(id, studStorage)){
                case Success(studentId) => complete(StatusCodes.OK, s"Student with id=$studentId successful delete")
                case Failure(ex)    => complete(StatusCodes.BadRequest, s"An error occurred: ${ex.getMessage}")
              }
            }
          }
        )
        case _ => complete(StatusCodes.Unauthorized -> "Admin unauthorized")
      }
      }
    )
  }
}
