package studServices.storage

import slick.jdbc.H2Profile
import studServices.logic.structures.{Mentor, Student}

import scala.concurrent.Future

trait ServiceStorage {
  /**
   * Генерирует уникальный id
   *
   * @return возвращает уникальный id
   */
  def generateID(forPerson: Boolean): Future[Int]


  def registryMentor(login: String,
                      password: String,
                      lastName: String,
                      firstName: String,
                      placePrefer: String,
                      subject: String,
                      phone: Option[String] = None): Future[Mentor]

  /**
   * Внести нового студента в БД
   *
   * @return возвращает нового пользователя
   */
  def registryStudent(login: String,
                      password: String,
                      lastName: String,
                      firstName: String,
                      placePrefer: String,
                      phone: Option[String] = None): Future[Student]

//  /**
//   * Проверить логин и пароль пользователя
//   *
//   * @return возвращает true, если логин и пароль введены верно, иначе - false
//   */
//  def checkAuthData(): Future[Boolean]

  val storage: H2Profile.backend.Database
}