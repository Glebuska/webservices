package studServices.storage

import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._
import studServices.logic.structures.{Mentor, Student}

import scala.concurrent.{ExecutionContext, Future}
import scala.math.abs

class ServiceStorageImp(storageDB: H2Profile.backend.Database)(implicit ec: ExecutionContext) extends ServiceStorage {
  /**
   * Генерирует уникальный id
   *
   * @return возвращает уникальный id
   */
  override def generateID(forPerson: Boolean): Future[Int] = {
    val id = abs(scala.util.Random.nextInt())
    val getIdQuery = if (forPerson){
        sql"""
           select PERSON_ID from PERSON
           where PERSON_ID = $id
           """
        .as[Int].headOption
    }
    else {
        sql"""
           select STUDENT_ID from Student
           where STUDENT_ID = $id
           """.as[Int].headOption
    }
    storage.run(getIdQuery).flatMap{
      case Some(_) => generateID(forPerson)
      case _ => Future.successful(id)
    }

  }

  /**
   * Внести студента в БД
   *
   * @return возвращает нового студента
   */
  override def registryStudent(login: String,
                               password: String,
                               lastName: String,
                               firstName: String,
                               placePrefer: String,
                               phone: Option[String] = None): Future[Student] = {
    val personIdF = generateID(true)
    val studentIdF = generateID(forPerson = false)
    val res = for {
        personId <- personIdF
        studentId <- studentIdF
        queryIntoPerson =
        sqlu"""
           INSERT INTO PERSON(PERSON_ID, Login, Password, Last_Name, First_Name, Phone) VALUES ($personId, ${login}, ${password}, ${lastName}, ${firstName}, ${phone} )
           """
        queryIntoStudent =
        sqlu"""
           INSERT INTO Student (Student_ID, Person_ID) VALUES ($studentId, $personId)
          """
          unionQuery = DBIO.seq(queryIntoPerson, queryIntoStudent)
                         _ <- storage.run(unionQuery)
      }
        yield Student(personId, login, password, lastName, firstName, placePrefer, phone, studentId)
      res
    }

  override def registryMentor(login: String, password: String, lastName: String, firstName: String, placePrefer: String, subject: String, phone: Option[String]): Future[Mentor] = ???

  override val storage = storageDB
}

