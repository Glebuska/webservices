package studServices.logic


import slick.jdbc.GetResult
import studServices.api.AuthenticationUtil.LoginRequest
import studServices.logic.structures.{AdminImp, Mentor, Order, RegistrationRequest, Student}

object JsonPrepare {
  import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
  import io.circe.{Decoder, Encoder}

  implicit val getMentorResult: AnyRef with GetResult[Mentor] = GetResult(r => Mentor(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))
  implicit val jsonMentorEncoder: Encoder[Mentor] = deriveEncoder[Mentor]
  implicit val jsonMentorDecoder: Decoder[Mentor] = deriveDecoder[Mentor]

  implicit val getStudentResult: AnyRef with GetResult[Student] = GetResult(r => Student(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))

  implicit val jsonStudentEncoder: Encoder[Student] = deriveEncoder
  implicit val jsonStudentDecoder: Decoder[Student] = deriveDecoder


  implicit val jsonAdminEncoder: Encoder[AdminImp] = deriveEncoder[AdminImp]
  implicit val jsonAdminDecoder: Decoder[AdminImp] = deriveDecoder[AdminImp]

  implicit val jsonLoginRequestEncoder: Encoder[LoginRequest] = deriveEncoder[LoginRequest]
  implicit val jsonLoginRequestDecoder: Decoder[LoginRequest] = deriveDecoder[LoginRequest]

  implicit val getOrderResult: AnyRef with GetResult[Order] = GetResult(r => Order(r.<<, r.<<, r.<<, r.<<, r.<<))

  implicit val jsonRegistrationRequestEncoder: Encoder[RegistrationRequest] = deriveEncoder[RegistrationRequest]
  implicit val jsonRegistrationRequestDecoder: Decoder[RegistrationRequest] = deriveDecoder[RegistrationRequest]
}
