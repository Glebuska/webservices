package studServices.logic.structures

import studServices.storage.ServiceStorage

import scala.concurrent.Future

trait Admin extends User {
  /**
   * Возвращает все заказы
   *
   * @return все заказы
   */
  override def findOrders(serviceStorage: ServiceStorage): Future[Vector[Order]]

  /**
   * Возвращает все активные заказы
   *
   * @return все активные заказы
   */
  override def findActiveOrders(serviceStorage: ServiceStorage): Future[Vector[Order]]

  /**
   * Возвращает заказ с данным идентификатором
   *
   * @param id идентификатор заказа
   * @return заказ или None, если заказ не найден
   */
  override def findOrdersById(id: Int, serviceStorage: ServiceStorage): Future[Order]

  /**
   * Возвращает всех студентов
   *
   * @return список студентов
   */

  def findStudents(serviceStorage: ServiceStorage): Future[Vector[Student]]

  /**
   * Возвращает всех менторов
   *
   * @return список менторов
   */

  def findMentors(serviceStorage: ServiceStorage): Future[Vector[Mentor]]

  /**
   * Удаляет студента из БД
   *
   * @param id идентификатор студента
   * @return id удаленного студента или StudentNotFoundException
   */
  def deleteStudent(id: Int, serviceStorage: ServiceStorage): Future[Int]

  /**
   * Удаляет ментора из БД
   *
   * @param id идентификатор ментора
   * @return id удаленного ментора или MentorNotFoundException
   */
  def deleteMentor(id: Int, serviceStorage: ServiceStorage): Future[Int]
}
