package studServices.logic.structures

final case class RegistrationRequest(login: String
                                     , password: String
                                     , lastName: String
                                     , firstName: String
                                     , placePrefer: String)
