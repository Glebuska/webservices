package studServices.logic.structures

import slick.jdbc.H2Profile.api._
import studServices.{MentorNotFoundException, OrderNotFoundException}
import studServices.logic.JsonPrepare.getMentorResult
import studServices.storage.ServiceStorage

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.math.abs

final case class Student(personId: Int,
                         login: String,
                         password: String,
                         lastName: String,
                         firstName: String,
                         placePrefer: String,
                         phone: Option[String],
                         studentId: Int) extends Person {

  import studServices.logic.JsonPrepare.getOrderResult

  /**
   * Возвращает все заказы пользователя
   *
   * @return все заказы пользователя или None, если ничего нет
   */
  override def findOrders(studStorage: ServiceStorage): Future[Vector[Order]] = {
    val query =
      sql"""
           select "Order".ORDER_ID, "Order".SUBJECT, "Order".PLACE_ORDER, "Order".ORDER_TYPE_ID, "Order".COMMENTS
            from "Order", STUDENT_ORDERS
            where "Order".ORDER_ID = STUDENT_ORDERS.ORDER_ID
            and STUDENT_ORDERS.STUDENT_ID = ${this.studentId}
           """.as[Order]

    studStorage.storage.run(query)
  }

  /**
   * Возвращает все активные заказы пользователя
   *
   * @return все активные заказы пользователя или None, если ничего нет
   */
  override def findActiveOrders(serviceStorage: ServiceStorage): Future[Vector[Order]] = {
    val query =
      sql"""
           select "Order".ORDER_ID, "Order".SUBJECT, "Order".Place_ORDER, "Order".ORDER_TYPE_ID, "Order".COMMENTS
           from "Order", STUDENT_ORDERS
           where "Order".ORDER_ID = STUDENT_ORDERS.ORDER_ID
           and STUDENT_ORDERS.STUDENT_ID = ${this.studentId}
           and "Order".ORDER_TYPE_ID = 1
           """.as[Order]
    serviceStorage.storage.run(query)
  }


  /**
   * Найти напарника(если ментор - студента, если студент - ментора) по предмету
   *
   * @param subject предмет для фильтрации
   * @return возвращает список результата или None, если никого нет
   */
  override def findPartner(subject: String, serviceStorage: ServiceStorage): Future[Vector[Mentor]] = {
    val query =
      sql"""select Person.PERSON_ID, LOGIN, PASSWORD, LAST_NAME, FIRST_NAME, PLACE_PREFER, PHONE, MENTOR_ID, Subject, AVG_MARK
            from PERSON, MENTOR
            where PERSON.PERSON_ID = MENTOR.MENTOR_ID and MENTOR.SUBJECT = $subject""".as[Mentor]

    serviceStorage.storage.run(query)
  }

  /**
   * Отправляет запрос на сделку для пользователя
   *
   * @param mentorID выбранный пользователь
   * @return возвращает новый заказ
   */
  def sendRequest(mentorID: Int, serviceStorage: ServiceStorage): Future[Order] = {
    val idF = generateID(serviceStorage)
    val findMentorQuery =
      sql"""select PERSON.PERSON_ID, LOGIN, PASSWORD, LAST_NAME, FIRST_NAME, PLACE_PREFER, PHONE, MENTOR_ID, SUBJECT, AVG_MARK
            from PERSON, MENTOR
           where MENTOR_ID = $mentorID """.as[Mentor].headOption

    val mentorF: Future[Mentor] = serviceStorage.storage.run(findMentorQuery).flatMap{
      case None => Future.failed(MentorNotFoundException(mentorID))
      case Some(value) => Future.successful(value)
    }
    for {
      id <- idF
      mentor <- mentorF
      query =
          sqlu"""insert into "Order"(ORDER_ID, SUBJECT, PLACE_ORDER, ORDER_TYPE_ID)
                VALUES ($id, ${mentor.subject}, ${this.placePrefer}, 1)"""
      _ <- serviceStorage.storage.run(query)
    } yield Order(id, mentor.subject, this.placePrefer, 1, None)
  }


  /**
   * Возвращает заказ с данным идентификатором
   *
   * @param id идентификатор заказа
   * @return заказ или None, если заказ не найден
   */
  override def findOrdersById(id: Int, serviceStorage: ServiceStorage): Future[Order] = {
    val query =
      sql"""
            select "Order".ORDER_ID, SUBJECT, PLACE_ORDER, ORDER_TYPE_ID, COMMENTS
            from "Order", STUDENT_ORDERS
            where STUDENT_ORDERS.ORDER_ID = "Order".ORDER_ID
              and STUDENT_ORDERS.STUDENT_ID = ${this.studentId}
          """
        .as[Order].headOption

    serviceStorage.storage.run(query).flatMap {
      case None => Future.failed(OrderNotFoundException(id))
      case Some(order) => Future.successful(order)
    }
  }

  private def generateID(storage: ServiceStorage): Future[Int] = {
    val id = abs(scala.util.Random.nextInt())
    val getIdQuery =
      sql"""
           select ORDER_ID from "Order"
           where ORDER_ID = $id
           """
        .as[Int].headOption

    storage.storage.run(getIdQuery).flatMap {
      case Some(_) => generateID(storage)
      case _ => Future.successful(id)
    }
  }
}