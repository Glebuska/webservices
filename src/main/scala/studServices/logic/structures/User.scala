package studServices.logic.structures

import studServices.storage.ServiceStorage

import scala.concurrent.Future

trait User {
  def login: String
  def password: String

  /**
   * Возвращает все заказы
   *
   * @return все заказы
   */
  def findOrders(serviceStorage: ServiceStorage): Future[Vector[Order]]

  /**
   * Возвращает все активные заказы
   *
   * @return все активные заказы или None, если ничего нет
   */
  def findActiveOrders(serviceStorage: ServiceStorage): Future[Vector[Order]]

  /**
   * Возвращает заказ с данным идентификатором
   *
   * @param id идентификатор заказа
   * @return заказ или None, если заказ не найден
   */
  def findOrdersById(id: Int, serviceStorage: ServiceStorage): Future[Order]

}
