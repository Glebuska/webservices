package studServices.logic.structures

import studServices.storage.ServiceStorage

import scala.concurrent.Future


trait Person extends User {
    override def login: String
    override def password: String
    val personId: Int
    val lastName: String
    val firstName: String
    val placePrefer: String
    val phone: Option[String]


    /**
     * Возвращает все заказы пользователя
     *
     * @return все заказы
     */
    override def findOrders(serviceStorage: ServiceStorage): Future[Vector[Order]]

    /**
     * Возвращает все активные заказы
     *
     * @return все активные заказы или None, если ничего нет
     */
    override def findActiveOrders(serviceStorage: ServiceStorage): Future[Vector[Order]]


    def findPartner(subject: String, serviceStorage: ServiceStorage): Future[Vector[Person]]

  }
