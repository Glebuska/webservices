package studServices.logic.structures

  case class Order(id: Int,
                   subject: String,
                   place: String,
                   statusId: Int, //better is ENUM, but idk how use it in sql
                   comments: Option[String])

