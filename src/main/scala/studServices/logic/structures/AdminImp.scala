package studServices.logic.structures

import slick.jdbc.H2Profile.api._
import studServices.ServiceHttpApp.ec
import studServices.logic.JsonPrepare.getStudentResult
import studServices.storage.ServiceStorage
import studServices.{MentorNotFoundException, OrderNotFoundException, StudentNotFoundException}

import scala.concurrent.Future



  case class AdminImp(login: String = "admin", password: String = "admin") extends Admin {
    import studServices.logic.JsonPrepare.{getMentorResult, getOrderResult}
    /**
     * Возвращает все заказы
     *
     * @return все заказы
     */
    override def findOrders(serviceStorage: ServiceStorage): Future[Vector[Order]] = {
      val query = sql"""select * from "Order" """.as[Order]
      serviceStorage.storage.run(query)
    }

    /**
     * Возвращает все активные заказы
     *
     * @return все активные заказы
     */
    override def findActiveOrders(serviceStorage: ServiceStorage): Future[Vector[Order]] = {
      val activeOrdersConst = 1
      val query =
        sql"""select ORDER_ID, SUBJECT, PLACE_ORDER, ORDER_TYPE_ID, COMMENTS
           from "Order", ORDER_STATUS
           where "Order".ORDER_TYPE_ID = ORDER_STATUS.TYPE_ID
           and ORDER_STATUS.TYPE_ID = $activeOrdersConst""".as[Order]
      serviceStorage.storage.run(query)
    }


    /**
     * Возвращает заказ с данным идентификатором
     *
     * @param id идентификатор заказа
     * @return заказ или OrdersNot, если заказ не найден
     */
    override def findOrdersById(id: Int, serviceStorage: ServiceStorage): Future[Order] = {
      val query =
        sql"""select * from "Order"
           where ORDER_ID = $id """.as[Order].headOption
      serviceStorage.storage.run(query).flatMap {
        case None => Future.failed(OrderNotFoundException(id))
        case Some(value) => Future.successful(value)
      }
    }

    /**
     * Возвращает всех студентов
     *
     * @return список студентов
     */
    override def findStudents(serviceStorage: ServiceStorage): Future[Vector[Student]] = {
      val query =
        sql"""select Person.PERSON_ID, LOGIN, PASSWORD, LAST_NAME, FIRST_NAME, PLACE_PREFER, PHONE, STUDENT_ID
           from PERSON, STUDENT
           where PERSON.PERSON_ID = STUDENT.PERSON_ID """
          .as[Student]
      serviceStorage.storage.run(query)
    }

    /**
     * Возвращает всех менторов
     *
     * @return список менторов
     */
    override def findMentors(serviceStorage: ServiceStorage): Future[Vector[Mentor]] = {
      val query =
        sql"""select PERSON.PERSON_ID, LOGIN, PASSWORD, LAST_NAME, FIRST_NAME, PLACE_PREFER, PHONE, MENTOR_ID, SUBJECT, AVG_MARK
           from PERSON, MENTOR
           where PERSON.PERSON_ID = MENTOR.PERSON_ID"""
          .as[Mentor]
      serviceStorage.storage.run(query)
    }

    /**
     * Удаляет студента из БД
     *
     * @param id идентификатор студента
     * @return id удаленного студента или StudentNotFoundException
     */
    override def deleteStudent(id: Int, serviceStorage: ServiceStorage): Future[Int] = {
      val sqlPersonIdQuery =
        sql"""
             select Person.PERSON_ID FROM PERSON
             WHERE PERSON.PERSON_ID = $id
           """
          .as[Int].headOption
      serviceStorage.storage.run(sqlPersonIdQuery).flatMap {
        case None => Future.failed(StudentNotFoundException(id))
        case Some(value) =>
          val sqlDeleteQuery =
            sqlu"""
            delete from PERSON where PERSON_ID = $value
            """
          serviceStorage.storage.run(sqlDeleteQuery).map(_ => id)
      }

    }

    /**
     * Удаляет ментора из БД
     *
     * @param id идентификатор ментора
     * @return id удаленного ментора или MentorNotFoundException
     */
    override def deleteMentor(id: Int, serviceStorage: ServiceStorage): Future[Int] = {
      val sqlPersonIdQuery =
        sql"""
             select Person.PERSON_ID FROM PERSON, MENTOR
             WHERE PERSON.PERSON_ID = $id
           """
          .as[Int].headOption
      serviceStorage.storage.run(sqlPersonIdQuery).flatMap {
        case None => Future.failed(MentorNotFoundException(id))
        case Some(value) =>
          val sqlDeleteQuery =
            sqlu"""
            delete from PERSON where $value = $id
            """
          serviceStorage.storage.run(sqlDeleteQuery).map(_ => id)
      }
    }
  }



