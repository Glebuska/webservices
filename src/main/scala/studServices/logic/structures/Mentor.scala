package studServices.logic.structures
import studServices.storage.ServiceStorage

import scala.concurrent.Future

final case class Mentor(personId: Int, login: String, password: String, lastName: String, firstName: String, placePrefer: String, phone: Option[String], mentorId: Int, subject: String, avgMark: Double) extends Person {
  /**
   * Возвращает все заказы пользователя
   *
   * @return все заказы
   */
  override def findOrders(serviceStorage: ServiceStorage): Future[Vector[Order]] = ???

  /**
   * Возвращает все активные заказы
   *
   * @return все активные заказы или None, если ничего нет
   */
  override def findActiveOrders(serviceStorage: ServiceStorage): Future[Vector[Order]] = ???

  override def findPartner(subject: String, serviceStorage: ServiceStorage): Future[Vector[Person]] = ???

  /**
   * Возвращает заказ с данным идентификатором
   *
   * @param id идентификатор заказа
   * @return заказ или None, если заказ не найден
   */
  override def findOrdersById(id: Int, serviceStorage: ServiceStorage): Future[Order] = ???
}





