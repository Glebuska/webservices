package studServices

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

sealed abstract class ServiceException(message: String) extends Exception(message)

final case class OrderNotFoundException(orderId: Int) extends ServiceException(s"Order with id=$orderId not found")
final case class StudentNotFoundException(orderId: Int) extends ServiceException(s"Student with id=$orderId not found")
final case class MentorNotFoundException(orderId: Int) extends ServiceException(s"Mentor with id=$orderId not found")
final case class LoginExistException(login: String) extends ServiceException(s"People with login=$login already exist")

object ServiceException{
  implicit val jsonOrderExceptionEncoder: Encoder[OrderNotFoundException] = deriveEncoder
  implicit val jsonOrderExceptionDecoder: Decoder[OrderNotFoundException] = deriveDecoder

  implicit val jsonStudentExceptionEncoder: Encoder[StudentNotFoundException] = deriveEncoder
  implicit val jsonStudentExceptionDecoder: Decoder[StudentNotFoundException] = deriveDecoder

  implicit val jsonMentorExceptionEncoder: Encoder[MentorNotFoundException] = deriveEncoder
  implicit val jsonMentorExceptionDecoder: Decoder[MentorNotFoundException] = deriveDecoder
}