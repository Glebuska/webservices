CREATE TABLE IF NOT EXISTS Person(
                                     Person_ID         INTEGER      IDENTITY PRIMARY KEY NOT NULL,
                                     Login           VARCHAR(20) NOT NULL,
                                     Password        VARCHAR(20) NOT NULL,
                                     Last_Name	      VARCHAR(20)  NOT NULL,
                                     First_Name	      VARCHAR(20)  NOT NULL,
                                     Time_Prefer		  DATE         NOT NULL DEFAULT CURRENT_DATE,
                                     Place_Prefer	  VARCHAR(20)		DEFAULT 'University',
                                     Phone	          VARCHAR(20)       NOT NULL  DEFAULT ''
);


CREATE TABLE IF NOT EXISTS Student(
                                      Student_ID              INTEGER         NOT NULL IDENTITY PRIMARY KEY,
                                      Person_ID               INTEGER         NOT NULL
);



CREATE TABLE IF NOT EXISTS Student_Orders(
                                             Student_ID              INTEGER         NOT NULL,
                                             Order_ID				INTEGER         NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS Mentor(
                                     Mentor_ID				INTEGER         NOT NULL IDENTITY PRIMARY KEY,
                                     Person_ID				INTEGER         NOT NULL ,
                                     Avg_Mark                 FLOAT
);

CREATE TABLE IF NOT EXISTS Mentor_Orders(
                                            Mentor_ID              INTEGER         NOT NULL,
                                            Order_ID				  INTEGER         NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS Order_Status(
                                           Type_ID			 INTEGER         NOT NULL PRIMARY KEY,
                                           Order_Status      VARCHAR(30)                 NOT NULL

);

CREATE TABLE IF NOT EXISTS "Order"(
                                      Order_ID                      INTEGER         NOT NULL IDENTITY PRIMARY KEY,
                                      Subject                       VARCHAR(30)     NOT NULL,
                                      Time_Order                    DATETIME        NOT NULL,
                                      Order_Type_Id				INTEGER         NOT NULL,
                                      Mark						     INTEGER,
                                      Comments                      VARCHAR(50),
                                      CONSTRAINT Order_PK PRIMARY KEY (Order_ID)
)
;

-------------------------------------------------------------
-- Создание FK
-------------------------------------------------------------
ALTER TABLE Student ADD CONSTRAINT FK_Student_Person
    FOREIGN KEY (Person_ID)
        REFERENCES Person(Person_ID)
;

ALTER TABLE Mentor ADD CONSTRAINT FK_Mentor_Person
    FOREIGN KEY (Person_ID)
        REFERENCES Person(Person_ID)
;


ALTER TABLE Student_Orders ADD CONSTRAINT FK_Student_Order
    FOREIGN KEY (Student_ID)
        REFERENCES Student(Student_ID);

ALTER TABLE Mentor_Orders ADD CONSTRAINT FK_Mentor_Order
    FOREIGN KEY (Mentor_ID)
        REFERENCES Mentor(Mentor_ID)
;


ALTER TABLE Student_Orders ADD CONSTRAINT FK_Order_Student
    FOREIGN KEY (Order_ID)
        REFERENCES "Order"(Order_ID)
;

ALTER TABLE Mentor_Orders ADD CONSTRAINT FK_Order_Mentor
    FOREIGN KEY (Order_ID)
        REFERENCES "Order"(Order_ID)
;

ALTER TABLE "Order" ADD CONSTRAINT FK_Order_Status
    FOREIGN KEY (Order_Type_Id)
        REFERENCES Order_Status(Type_ID)
;
---------------------------------------------------------------
--Заполнение таблиц тестовыми данными
INSERT INTO PERSON VALUES (1, 'gleb.mirzazyanov', 'qwerty', 'Mirzazyanov', 'Gleb', '2020-09-08', 'home', '+7 (904) 4058016');
INSERT INTO PERSON(Login, Password, Last_Name, First_Name, Phone) VALUES ('IVANOV_ANATOLY', 'qwerty12345', 'Ivanov', 'Ananoly', '+7 (911) 3453410');
INSERT INTO PERSON(Login, Password, Last_Name, First_Name, Time_Prefer, Phone) VALUES ('ARTEMIEV_ARTEM', '12345', 'Artemiev', 'Artem', '2020-09-08', '+7 (921) 40123016');
INSERT INTO PERSON(Login, Password, Last_Name, First_Name, Phone) VALUES ('SegevEGOR', 'egorka123', 'Segeev', 'Egor', '+7 (904) 40580111');
INSERT INTO PERSON(Login, Password, Last_Name, First_Name, Phone) VALUES ('MilosLyba', '123456', 'Miloserdova', 'Lyubov', '+7 (904) 40515234');
INSERT INTO PERSON(Login, Password, Last_Name, First_Name, Time_Prefer, Phone) VALUES ('MaxBaclan', '1111', 'Baclanovskiy', 'Maxim', '2020-11-08', '+7 (904) 43432423');
INSERT INTO PERSON(Login, Password, Last_Name, First_Name, Phone) VALUES ('ant', 'ant1234', 'Terechov', 'Andrey', '+7 (993) 43001003');
INSERT INTO PERSON(Login, Password, Last_Name, First_Name, Phone) VALUES ('ssu', 'sdf2', 'Sartasov', 'Stanislav', '+7 (930) 4355555');

INSERT INTO Student (Student_ID, Person_ID) VALUES (1, 1);
INSERT INTO Student (Person_ID) VALUES (2);
INSERT INTO Student (Person_ID) VALUES (3);
INSERT INTO Student (Person_ID) VALUES (4);

INSERT INTO Mentor (Mentor_ID, Person_ID, Avg_Mark) VALUES (1, 6, 5.0);
INSERT INTO Mentor VALUES (2, 7, 4.75 );
INSERT INTO Mentor VALUES (3, 8, 4.90 );

INSERT INTO Order_Status(Order_Status, Type_ID)VALUES ( 'Await', 1 );
INSERT INTO Order_Status(Order_Status, Type_ID)VALUES ( 'In progress', 2 );
INSERT INTO Order_Status(Order_Status, Type_ID)VALUES ( 'Done', 3 );

INSERT INTO "Order"(ORDER_ID, Subject, Time_Order, Order_Type_Id)
VALUES (1, 'algebra', '2020-09-10 16:00', 1);
INSERT INTO "Order"(ORDER_ID, Subject, Time_Order, Order_Type_Id)
VALUES (2, 'probability theory', '2020-09-10 18:00', 2);
INSERT INTO "Order"(ORDER_ID, Subject, Time_Order, Order_Type_Id)
VALUES (3, 'programming', '2020-12-10 16:00', 2);
INSERT INTO "Order"(ORDER_ID, Subject, Time_Order, Order_Type_Id)
VALUES (4, 'mat analysis', '2020-12-10 20:00', 3);

INSERT INTO Student_Orders(Student_ID, Order_ID) VALUES ( 1, 1 );
INSERT INTO Student_Orders(Student_ID, Order_ID) VALUES ( 1, 2 );
INSERT INTO Student_Orders(Student_ID, Order_ID) VALUES ( 2, 3 );
INSERT INTO Student_Orders(Student_ID, Order_ID) VALUES ( 3, 4 );

INSERT INTO Mentor_Orders VALUES ( 1, 1 );
INSERT INTO Mentor_Orders VALUES ( 2, 3 );
INSERT INTO Mentor_Orders VALUES ( 3, 4 );
INSERT INTO Mentor_Orders VALUES ( 3, 2 )
