package studServices.api

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpHeader, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import io.circe.generic.auto.exportDecoder
import org.scalamock.scalatest.MockFactory
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import studServices.OrderNotFoundException
import studServices.logic.structures.{AdminImp, Mentor, Order, Student}
import studServices.storage.ServiceStorage

import scala.concurrent.Future

class AdminApiSpec extends AnyFunSpec with ScalatestRouteTest with MockFactory {
  import AuthenticationUtil.AccessTokenHeaderName
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
  import studServices.logic.JsonPrepare.jsonAdminEncoder
  describe("POST /admin/login") {
    it("проверка возможности авторизации") {
      Post("/admin/login", AdminImp())~> route ~> check {
        header(AccessTokenHeaderName) shouldBe Some(_: HttpHeader)
        assert(status == StatusCodes.OK)
      }
    }
  }

  describe("GET /admin/orders") {
    it("возвращает все ордера в системе") {
      (mockAdminImplementation.findOrders _)
        .expects(mockServiceStorage)
        .returns(Future.successful(Vector(order1, order2)))

    Post("/admin/login", AdminImp()) ~> route ~> check {
        header(AccessTokenHeaderName).map { accessTokenHeader =>
          Get("/admin/orders").addHeader(RawHeader("Authorization", accessTokenHeader.value())) ~> route ~> check {
            assert(status == StatusCodes.OK)
            assert(responseAs[Vector[Order]].contains(order1))
            assert(responseAs[Vector[Order]].contains(order2))
            assert(responseAs[Vector[Order]].length == 2)
          }
        }
      }

    }
  }

  describe("GET /admin/orders/active") {
    it("возвращает все активные ордера в системе") {
      (mockAdminImplementation.findActiveOrders _)
        .expects(mockServiceStorage)
        .returns(Future.successful(Vector(order1)))

      Post("/admin/login", AdminImp()) ~> route ~> check {
        header(AccessTokenHeaderName).map { accessTokenHeader =>
          Get("/admin/orders/active").addHeader(RawHeader("Authorization", accessTokenHeader.value())) ~> route ~> check {
            assert(status == StatusCodes.OK)
            assert(responseAs[Vector[Order]].contains(order1))
            assert(!responseAs[Vector[Order]].contains(order2))
            assert(responseAs[Vector[Order]].length == 1)
          }
        }
      }

    }
  }

  describe("GET /admin/orders/{id}") {
    it("Успешно находит заказ по id") {
      (mockAdminImplementation.findOrdersById _)
        .expects(1, mockServiceStorage)
        .returns(Future.successful(order1))

      Post("/admin/login", AdminImp()) ~> route ~> check {
        header(AccessTokenHeaderName).map { accessTokenHeader =>
          Get("/admin/orders/1").addHeader(RawHeader("Authorization", accessTokenHeader.value())) ~> route ~> check {
            assert(status == StatusCodes.OK)
            assert(responseAs[Option[Order]].isDefined)
          }
        }
      }
    }

    it("Не находит заказ") {
      (mockAdminImplementation.findOrdersById _)
        .expects(2, mockServiceStorage)
        .returns(Future.failed(OrderNotFoundException(1)))

      Post("/admin/login", AdminImp()) ~> route ~> check {
        header(AccessTokenHeaderName).map { accessTokenHeader =>
          Get("/admin/orders/2").addHeader(RawHeader("Authorization", accessTokenHeader.value())) ~> route ~> check {
            assert(status == StatusCodes.BadRequest)
          }
        }
      }
    }

  }

  describe("GET /admin/mentors") {
    it("Успешно находит всех менторов") {
      (mockAdminImplementation.findMentors _)
        .expects(mockServiceStorage)
        .returns(Future.successful(Vector(mentor1, mentor2)))

      Post("/admin/login", AdminImp()) ~> route ~> check {
        header(AccessTokenHeaderName).map { accessTokenHeader =>
          Get("/admin/mentors").addHeader(RawHeader("Authorization", accessTokenHeader.value())) ~> route ~> check {
            assert(status == StatusCodes.OK)
            assert(responseAs[Vector[Mentor]].contains(mentor2))
            assert(responseAs[Vector[Mentor]].contains(mentor1))
            assert(responseAs[Vector[Mentor]].length == 2)
          }
        }
      }

    }
  }


  describe("GET /admin/students") {
    it("Успешно находит всех студентов") {
      (mockAdminImplementation.findStudents _)
        .expects(mockServiceStorage)
        .returns(Future.successful(Vector(student1, student2)))

      Post("/admin/login", AdminImp()) ~> route ~> check {
        header(AccessTokenHeaderName).map { accessTokenHeader =>
          Get("/admin/students").addHeader(RawHeader("Authorization", accessTokenHeader.value())) ~> route ~> check {
            assert(status == StatusCodes.OK)
            assert(responseAs[Vector[Student]].contains(student1))
            assert(responseAs[Vector[Student]].contains(student2))
            assert(responseAs[Vector[Student]].length == 2)
          }
        }
      }
    }
  }

  describe("DELETE /admin/student/id") {
    it("Удаляет студента по id") {
      (mockAdminImplementation.deleteStudent _)
        .expects(1, mockServiceStorage)
        .returns(Future.successful(1))

      Post("/admin/login", AdminImp()) ~> route ~> check {
        header(AccessTokenHeaderName).map { accessTokenHeader =>
          Delete("/admin/student/1").addHeader(RawHeader("Authorization", accessTokenHeader.value())) ~> route ~> check {
            assert(status == StatusCodes.OK)
          }
        }
      }
    }
  }

  private val order1 = Order(1, "programming", "home", 1, None)
  private val order2 = Order(2, "matem", "cafe", 2, None)

  private val mentor1 = Mentor(1
                                , "glebuska"
                                , "qwerty1235"
                                , "Mirzazyanov"
                                , "Gleb"
                                , "University"
                                , None
                                , 1
                                , "cs"
                                , 4.78)
  private val mentor2 = Mentor(2
                                , "gerger"
                                , "qwerty1235"
                                , "Mirza"
                                , "Hleb"
                                , "Home"
                                , None
                                , 2
                                , "linear algebra"
                                , 3.78)

  private val student1 = Student(1
                                , "gerger"
                                , "qwerty1235"
                                , "Mirza"
                                , "Hleb"
                                , "Home"
                                , None
                                , 2)

  private val student2 = Student(2
                                , "gerger"
                                , "qwerty1235"
                                , "Mirza"
                                , "Hleb"
                                , "Home"
                                , None
                                , 3)

  private val mockServiceStorage = mock[ServiceStorage]
  private val mockAdminImplementation = mock[AdminImp]
  private val route = {
    Route.seal(
      new AdminApi(mockAdminImplementation, mockServiceStorage).route
    )(exceptionHandler = ServerExceptionHandler.exceptionHandler) // ExceptionHandler - обрабатывает ошибки, которые произошли при обработке запроса
  }

}
