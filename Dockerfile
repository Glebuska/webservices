FROM mozilla/sbt:latest

COPY . /web-service

WORKDIR web-service

CMD sbt run
